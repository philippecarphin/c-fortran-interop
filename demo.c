#include <stdio.h>
#include "myapi.h"
void demo(float a, float b, float c){
    printf("calling 'double d = fortran_average(&a,&b,&c);' with a=%f, b=%f, c=%f\n", a,b,c);
    float d = average_floats(&a, &b, &c);
    printf("The returned double d has value %f\n", d);
}

int main(int argc, char **argv)
{
    demo(1,2,3);
    demo(1,2,4);
    return 0;
}
