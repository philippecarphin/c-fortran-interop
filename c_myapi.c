#include "myapi.h"
#include <stdlib.h>
#include <stdio.h>
#include <ISO_Fortran_binding.h>
float sum_floats(float *a, float *b, float *c){
    return *a + *b + *c;
}

float *make_c_array(int NI, int NJ){
    fprintf(stderr, "%s(%d, %d) called\n", __func__, NI, NJ);
    float *c_array = malloc(NI * NJ * sizeof(*c_array));
    float *cursor = c_array;
    int index = 0;

    for(int line = 0; line < NJ; line++){
        for(int col = 0; col < NI; col++){
            // c_array[line*NI + col] = line;
            *cursor++ = line + line/10.0 + line/100.0 + line/1000.0 + line/10000.0 + line/100000.0 + line/1000000.0;
            index++;
        }
    }
    fprintf(stderr, "%s(): Returning %ld\n", __func__, (long unsigned int) c_array);
    return c_array;
}

int make_c_array_out_arg(int NI, int NJ, float **c_array){
    fprintf(stderr, "%s() called\n", __func__);
    *c_array = malloc(NI * NJ * sizeof(*c_array));
    float *cursor = c_array;
    int index = 0;

    for(int line = 0; line < NJ; line++){
        for(int col = 0; col < NI; col++){
            // c_array[line*NI + col] = line;
            *cursor++ = line + line/10 + line/100;
            index++;
        }
    }
    fprintf(stderr, "%s(): Returning %ld\n", __func__, (long unsigned int) c_array);
    return 0;
}

