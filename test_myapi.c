#include <stdio.h>
#include "myapi.h"

#define SUCCESS 0
#define FAILURE 1
// Cross mark ✗ : HEX 0xe29c97
// Checkmark ✓  : HEX 0xe29c93
// I could put the unicode char itself in the string but for some
// reason this feels more portable.
#define FAIL printf("\033[31m\xe2\x9c\x97\033[1;37m %s()\033[0m \n%s:%d: fail: \n", __func__, __FILE__, __LINE__)
#define FAIL_PRINT(x) FAIL;putchar('\t');printf((x));putchar('\n')
#define SUCCESS_PRINT printf("\033[32m\xe2\x9c\x93\033[1;37m %s()\033[0m\n", __func__)


int test_average_floats()
{
    float a = 1;
    float b = 2;
    float c = 3;
    float result = average_floats(&a,&b,&c);
    float expected = (a+b+c)/3;
    if(result - expected > 0.0000001){
        printf(": Expected %f, got %f\n", expected, result);
        FAIL_PRINT("Result different from expected");
        return FAILURE;
    }

    SUCCESS_PRINT;
    return SUCCESS;
}

int test_sum_floats(){
    float a = 1;
    float b = 2;
    float c = 4;
    float result = sum_floats(&a,&b,&c);
    float expected = (1.0 + 2.0 + 4.0);
    if(result - expected > 0.0000001){
        printf(": Expected %f, got %f\n", expected, result);
        FAIL_PRINT("Result different from expected");
        return FAILURE;
    }
    SUCCESS_PRINT;
    return SUCCESS;
}

int main(int argc, char **argv){
    int nb_fail = 0;

    nb_fail += test_average_floats();
    nb_fail += test_sum_floats();

    return nb_fail;
}
