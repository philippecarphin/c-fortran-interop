program addNumbers
    use f_myapi

! This simple program adds two numbers
   implicit none

! Type declarations
   real :: a, b, c, result

! Executable statements
   a = 12.0
   b = 15.0
   c = 15.0
   result = average(a,b,c)
   call average(1,2,3)
   print *, 'The total is ', result

end program addNumbers
