cmake_minimum_required(VERSION 3.10)

if("${CMAKE_INSTALL_PREFIX}" STREQUAL "/usr/local")
    set(CMAKE_INSTALL_PREFIX ${CMAKE_BINARY_DIR}/install CACHE FILEPATH "CMake Installation prefix for ${PROJECT_NAME}" FORCE)
    message(STATUS "Setting CMAKE_INSTALL_PREFIX to ${CMAKE_INSTALL_PREFIX}")
endif()

project(frotran_test Fortran C)

add_library(myapi f_myapi.f90 c_myapi.c)

add_executable(cdemo demo.c)
target_link_libraries(cdemo PRIVATE myapi)

add_executable(fdemo demo.f90)
target_link_libraries(fdemo PRIVATE myapi)

add_executable(f_array_demo demo_array.f90)
target_link_libraries(f_array_demo PRIVATE myapi)

install(TARGETS myapi cdemo fdemo LIBRARY DESTINATION lib)
install(FILES myapi.h DESTINATION include)


include(CTest)
add_custom_target(check COMMAND CTEST_OUTPUT_ON_FAILURE=1 ${CMAKE_CTEST_COMMAND})

add_test(NAME test_f_array_demo COMMAND f_array_demo)
add_dependencies(check f_array_demo)

add_executable(test_myapi_c test_myapi.c)
target_link_libraries(test_myapi_c PRIVATE myapi)
add_test(NAME test_myapi_c COMMAND test_myapi_c)
add_dependencies(check test_myapi_c)

add_executable(test_myapi_f test_myapi.f90)
target_link_libraries(test_myapi_f PRIVATE myapi)
add_test(NAME test_myapi_f COMMAND test_myapi_f)
add_dependencies(check test_myapi_f)
