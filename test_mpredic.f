      FUNCTION test_humrel()
        INTEGER :: test_humrel
        real tt(3)
        real dpd(3)
        real results(3)
        data tt/20.0,25.0,28.7/
        data dpd/10.0,1.5,3.3/
        data results/52.05764,91.21964,82.00195/

C       CALL FUNCTION TO TEST
        call HUMREL(tt,dpd,3)

C       COMPARE RESULT WITH EXPECTED VALUE
C       AND REPORT SUCCESS OR FAIL TO CALLER
        IF(ALL(abs(dpd - results) < 0.00001)) THEN
            test_humrel=0
        ELSE
            test_humrel=1
        ENDIF
      END FUNCTION


      PROGRAM TESTS
        IMPLICIT NONE
        INTEGER nbfail
        INTEGER :: test_humrel

C       RUN ALL test_* FUNCTIONS AND COUNT FAILURES
        nbfail = 0
        nbfail = nbfail + test_humrel()


C       PRINT RESULT
        IF(nbfail > 0)THEN
            write(*,*) nbfail, " tests have failed"
        ELSE
            write(*,*) "ALL TESTS HAVE PASSED"
        ENDIF

C       REPORT RESULT TO CALLING PROGRAM
        CALL EXIT(nbfail)

      END PROGRAM

