module f_myapi
interface
    subroutine say_hello()
    end subroutine
end interface

contains
    REAL FUNCTION average(X,Y,Z) BIND(C,NAME="average_floats")
        REAL X,Y,Z,SUM
        SUM = X + Y + Z
        average = SUM /3.0
        RETURN
    END FUNCTION average
    function average2(a,b) BIND(C)
        ! Needed because of the C_FLOAT in real(C_FLOAT)
        use iso_c_binding
        implicit none
        real(C_FLOAT) :: a,b, average2
        average2 = (a + b)/2
    end function
    SUBROUTINE subroutine_sum(a,b,s)
        real :: a,b,s
        s = a + b
    END SUBROUTINE

end module
