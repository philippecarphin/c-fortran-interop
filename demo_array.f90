program demo_array
    use, intrinsic :: iso_c_binding
    implicit none
! This simple program adds two numbers

! Type declarations
    type(C_PTR) :: c_array
    REAL(C_FLOAT), POINTER, dimension(:,:) :: f_array
    INTEGER :: i
INTERFACE
    !
    ! ISO_C_BINDING declaration for C function
    !
    !     float *make_c_array(int NI, int NJ);
    !
    ! TODO Most sources talk about putting the array as an
    ! out-parameter of the function, making me think this
    ! is the "preferred" (whatever that means) way of doing
    ! this.
    !
    ! One reason is that you can declare just f_array
    ! as a dummy parameter and the runtime figures it out.
    !
    ! You can even pass the size of the array as a parameter
    ! of the function and use it in the declaration of the
    ! dummy parameter for the out array.
    type(C_PTR) FUNCTION make_c_array(NI, NJ) BIND(C)
        use, intrinsic :: ISO_C_BINDING
        integer(C_INT), VALUE :: NI, NJ
    END FUNCTION

END INTERFACE


!
! Call the function and store the pointer in c_array to get a
! bare pointer in the form of a type(C_PTR) variable.
!
c_array = make_c_array(10, 10)

!
! use C_F_POINTER() to cast this into a FORTAN array
! variable.
!
! In the declaration
!
!     REAL(C_FLOAT), POINTER, DIMENSION(:,:) :: f_array
!
! there must be as many ':' in the DIMENSION() part of the
! declaration as there are elements in the shape part of the
! call to C_F_POINTER.  This is called the "rank" of the array.
!
! A shape of (:,:,...,:) is called a "deferred shape
! of rank N" (where N would be the actual number of ':').
! It can only be used with the POINTER or ALLOCATABLE
! attributes.
!
call C_F_POINTER(c_array, f_array, [10,10])


print *, "FORTAN: The pointer is", c_array


print *, "Print f_array(i,:)"
do i = 1, 10
    write(*,*) i, f_array(i,:)
end do

print *, "Print f_array(:,i)"
do i = 1, 10
    write(*,*) i, f_array(:,i)
end do

end program demo_array
