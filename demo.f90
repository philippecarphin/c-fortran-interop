subroutine prog_sub()
    write (*,*) "prog_subroutine called"
end subroutine

function p_func()
    write (*,*) "p_func called"
    p_func = 3.1415
    return
end function

module mainmod

    contains
        real function p_m_func()
            write (*,*) "p_m_func called"
            p_m_func = 3.1415
            return
        end function
end module

program addNumbers

    use mainmod
    use f_myapi
! This simple program adds two numbers
   implicit none

    INTERFACE
        FUNCTION sum_floats(a,b,c) result(other_name) BIND(C)
            real :: a,b,c, other_name
        END FUNCTION
    END INTERFACE
! Type declarations
   real :: a, b, c, res
   real :: s
   real :: p_func


! Executable statements
   a = 12.0
   b = 15.0
   c = 15.0
   call prog_sub()
   res = sum_floats(a,b,c)
   print *, 'The total is ', res

   call subroutine_sum(2.0,3.0,s)
   print *, 'Subroutine sum = ', s
   res = average2(1.1, 2.2)
   write (*,*) "res = ", res
   write (*,*)  "average2(1.1, 2.2) = ", average2(1.1, 2.2)

   ! Calling naked function
   res = p_func()
   write (*,*) "res = p_func -> res = ", res
   ! This line makes the program stall
   ! write (*,*)  "p_func() = ", p_func()

   ! Calling function from module in the same file
   res = p_m_func()
   write (*,*) "res = p_m_func -> res = ", res
   ! This line makes my program stall
   ! write (*,*) "p_m_func = ", p_m_func()
   write (6,*) "This goes to STDOUT"
   write (0,*) "This goes to STDERR"
end program addNumbers
